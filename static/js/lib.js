import axios from 'axios'
import {Message,Loading } from "element-ui"
import router from '../../src/router'
import store from '../../src/vuex/store'
import publicAddr from './publicUrl'

class Axios{
  constructor(){
    this.init()
  }
  init(){
    axios.defaults.baseURL = '/credit/'
  }

  _setUserInfo(data){
    // 把请求的数据存入vuex
    store.commit('setUserInfo',data);
  }
  /**
   * 判断是否是登录
   * @param url
   * @returns {boolean}
   * @private
   */
  _isLogin(url){
    if(url != 'dologin'){
      // axios.defaults.headers = {'x-token': store.state.user.user.token.token};
      return false;
    }else{
      return true;
    }
  }

  /**
   * 下载
   */
  DownLoad(fileName){
    window.open('http://test.credit.chezhubaitiao.com/credit/distributor/down?file=' + fileName);
  }
  /**
   * 判断是否返回数据
   * @param data 接收到的数据
   * @returns {boolean}
   * @private
   */
  _isStatus(data){
    if(data.code == 100){
      router.push('/login');
      Message.error(data.message || '请重新登录！');
      return false
    }else if(data.code == "100011"){
      router.push('/login');
      Message.error(data.msg || '请重新登录！');
    }else if(data.code == "100014"){
      Message.info(data.msg || '权限变更！');
      return true
    }else if(data.code == "100015"){
      Message.info(data.msg || '权限刷新！');
      //location.reload();
      return true
    }else if(data.code != "100000"){
      Message.error(data.msg || '请重新登录！');
    }else {
      return true
    }
  }
  /**
   * 全局错误处理
   * @param data 传入错误的数据
   * @private
   */
  _error(data){
    console.log(data)
    Message.error('网络错误！');
  }
  /**
   * GET 请求 {es6解构赋值}
   * @param type 包含url信息
   * @param data 需要发送的参数
   * @returns {Promise}
   * @constructor
   */
  HttpGet({url},data) {
    console.log(data)
    // 创建一个promise对象
    let _loading = Loading.service({ fullscreen: true ,target:document.getElementById("main")})
    this._isLogin(url)
    this.promise = new Promise((resolve, reject)=> {
      axios.get(url,{params:data})
        .then((data) => {
          _loading.close()
          console.log("返回成功="+JSON.stringify(data))
          if(this._isStatus(data.data)){
            resolve(data.data);
          }
        })
        .catch((data) =>{
          _loading.close()
          this._error(data);
        })
    })
    return this.promise;
  };
  HttpPost(url,Data,urlData){
    // 判断是否加头部
    //this._isLogin(url);
    // 创建一个promise对象
    console.log("action="+url+"  请求数据=="+JSON.stringify(Data))
    let _loading = Loading.service({ fullscreen: true ,target:document.getElementById("main")})
    this.promise = new Promise((resolve, reject)=> {
      /*for(const item in urlData){
        url += '/' + urlData[item];
      };*/
      axios.post(url,Data)
        .then((data) => {
          console.log("返回成功="+JSON.stringify(data))
          if(this._isStatus(data.data)){
            resolve(data.data)
          }
          _loading.close()
          // 是否请求成功
          /*if(this._isStatus(data.data)){
            // 是否需要存数据
            if(this._isLogin(url)){
              this._setUserInfo(data)
            };
            resolve(data)
          };*/
        })
        .catch((data) =>{
          _loading.close()
          this._error(data);
        })
    });
    return this.promise;
  };
  HttpPostFile(url,e,Data){
    // 判断是否加头部
    //this._isLogin(url);
    // 创建一个promise对象
    console.log("请求数据=="+JSON.stringify(Data))
    let file = e.target.files[0]
    let config = {
      headers: {'Content-Type': 'multipart/form-data'}
    }
    let param = new FormData() // 创建form对象
    param.append('file', file, file.name) // 通过append向form对象添加数据
    //param.append('file', file, file.name)

    let _loading = Loading.service({ fullscreen: true ,target:document.getElementById("main")})
    this.promise = new Promise((resolve, reject)=> {
      /*for(const item in urlData){
        url += '/' + urlData[item];
      };*/
      axios.post(url,param,config)
        .then((data) => {
          console.log("上传成功="+JSON.stringify(data))
          if(this._isStatus(data.data)){
            resolve(data.data)
          }
          _loading.close()
          // 是否请求成功
          /*if(this._isStatus(data.data)){
            // 是否需要存数据
            if(this._isLogin(url)){
              this._setUserInfo(data)
            };
            resolve(data)
          };*/
        })
        .catch((data) =>{
          _loading.close()
          this._error(data);
        })
    });
    return this.promise;
  };
}
export default new Axios();
