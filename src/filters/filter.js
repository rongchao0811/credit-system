/***
 * 授信状态字典
 */
const auditFilter = function (value) {
  let auditMap = new Map([['0', '待授信'],
                           ['1', '授信待审'],
                           ['2', '已授信'],
                           ['3', '修改待审'],
                           ['4', '冻结待审'],
                           ['5', '已冻结']]);
  value = '' + value
console.log(value)
  return auditMap.get(value)
}
/**
 * 关系 字典
 * @param value
 */
const relationFilter = function (value) {
  let relationMap = new Map([['11', '导员'],
                           ['1', '配偶'],
                           ['2', '父亲'],
                           ['3', '母亲'],
                           ['7', '同学'],
                           ['8', '同事'],
                           ['9', '朋友']]);
  value = '' + value
  return relationMap.get(value)
}

/***
 * 布尔值
 * @type {Map}
 */

const boolFilter = function (value) {
  value = '' + value
  let boolMap = new Map([['0', '否'], ['1', '是'],['','否']])
  return boolMap.get(value)
}


const haveFilter=function (value) {
  value = '' + value
  let boolMap = new Map([['0', '无'], ['1', '有'],['','无']])
  return boolMap.get(value)
}

/***
 * 产品状态
 */

const productFilter = function (value) {

  value = '' + value
  let productMap = new Map([['0', '锁定'], ['1', '激活'], ])
  return productMap.get(value)
}
/**
 * 黑名单状态
 */
const blackListFilter = function (value) {
  value = '' + value
  let statusMap = new Map([['0', '待审批'], ['1', '通过'], ['2', '未通过'], ['3', '移除']])
  return statusMap.get(value)
}

/**
 * 渠道商类型
 */
const channleTypeFilter=function (v) {
  v=''+v;
  let channelMap=new Map([ ['0', '其他企业'],
                           ['1', '内资企业'],
                           ['2', '国有企业'],
                           ['3', '集团企业'],
                           ['4', '股份合作企业'],
                           ['5', '联营企业'],
                           ['6', '有限责任企业'],
                           ['7', '股份有限企业'],
                           ['8', '私营企业']])
  return channelMap.get(v)
}

/***
 * 合同状态
 */
const  contractStatusFilter=function (v) {
  v=''+v;
  let contractMap=new Map([['0',"未审核"],['2',"通过"],['1',"不通过"]])
  return contractMap.get(v)

}

/***
 * 利率类型
 */
const rateFilter=function (v) {
  v=''+v;
  let rateMap=new Map([['1','日利率'],['2','月利率'],['3','年利率']])
  return rateMap.get(v)
}

/***
 * 还款模式
 */
const repayModeFilter=function (v) {
  v=''+v;
  let repayModeMap=new Map([['1','等额本息'],['2','等额本金']])
  return repayModeMap.get(v)
}
const periodTypeFilter=function (v) {
  v=''+v;
  let periodTypeMap=new Map([['1','月'],['2','天']])
  return periodTypeMap.get(v)
}

 const  decisionStatusFilter=function (v) {
   v=''+v;
   let decisionTypeMap=new Map([["1","可以合作"],["2","不可以合作"],["3","有条件可以合作"],["",""]])
   return decisionTypeMap.get(v)
 }
/***
 * 尽调模式
 */
const decisionFilter=function (v) {
  v=''+v;
  let decisionMap=new Map([['1','完成'],['0','待尽调']])
  return decisionMap.get(v)
}


export {decisionFilter,decisionStatusFilter,periodTypeFilter,repayModeFilter,haveFilter,rateFilter,productFilter, boolFilter, auditFilter,blackListFilter,channleTypeFilter,contractStatusFilter}

