import Vue from 'vue'
import Router from 'vue-router'
// import Login from '@/components/Login'
// import Index from '@/components/index'

Vue.use(Router)

const router = new Router({
  routes: [
    { meta: {
      requireAuth: true},
      path: '/',
      component: resolve => require(['@/components/index'], resolve)
    },
    {
      path: '/index',
      name: 'index',
      component: resolve => require(['@/components/index'], resolve),
      meta: {
        requireAuth: true,
      },
      children: [
        {
          path: '/register',//渠道商登记
          name: 'register',
          component: resolve => require(
            ['@/components/channelManage/channelRegister'], resolve),
          meta: {
            requireAuth: true,
          },
        },
        {
          path: '/channelInfo/:channelId/:action',//渠道新增
          name: 'channelInfo',
          component: resolve => require(
            ['@/components/channelManage/channelInfo'], resolve),
          meta: {
            requireAuth: true,
          },
        },
        {
          path: '/channelGoods',//渠道商品维护
          name: 'channelGoods',
          component: resolve => require(
            ['@/components/channelManage/channelGoods'], resolve),
        },
        {
          path: '/channelGoodsQuery',//渠道产品查询
          name: 'channelGoodsQuery',
          component: resolve => require(
            ['@/components/channelManage/channelGoodsQuery'], resolve),
          meta: {
            requireAuth: true,
          },
        },
        {
          path: '/channelCreateGoods',//渠道产品列表新增
          name: 'channelCreateGoods',
          component: resolve => require(
            ['@/components/channelManage/channelCreateGoods'], resolve),
          meta: {
            requireAuth: true,
          },
        },
        {
          path: '/channelChange',//渠道商尽调
          name: 'channelChange',
          component: resolve => require(
            ['@/components/channelManage/channelChange'], resolve),
          meta: {
            requireAuth: true,
          },
        },
        {
          path: '/changeCreate/:channelId/:channelName/:action/:investId',//渠道商新增尽调
          name: 'changeCreate',
          meta: {
            requireAuth: true,
          },
          component: resolve => require(
            ['@/components/channelManage/changeCreate'], resolve),
        },
        {
          path: '/channelList/:channelId/:channelName/:channelDate',//尽调渠道商列表
          name: 'channelList',
          component: resolve => require(
            ['@/components/channelManage/channelList'], resolve),
          meta: {
            requireAuth: true,
          },
        },
        {
          path: '/channelVerb/',//渠道商授信
          name: 'channelVerb',
          component: resolve => require(
            ['@/components/channelManage/channelVerb'], resolve),
          meta: {
            requireAuth: true,
          },
        },
        {
          path: '/verbInfoAdapt/:id/:channelId/:operate',//授信调整
          name: 'verbInfoAdapt',
          component: resolve => require(
            ['@/components/channelManage/verbInfoAdapt'], resolve),
          meta: {
            requireAuth: true,
          },
        },

        {
          path: '/verbInfo/:channelId/:action',//渠道商授信信息
          name: 'verbInfo',
          component: resolve => require(
            ['@/components/channelManage/verbInfo'], resolve),
          meta: {
            requireAuth: true,
          },
        },
        {
          path: '/channelDeposit',//保证金管理
          name: 'channelDeposit',
          component: resolve => require(
            ['@/components/channelManage/channelDeposit'], resolve),
          meta: {
            requireAuth: true,
          },
        },
        {
          path: '/depositInfo/:channelId/:depositaction/:action',//保证金信息
          name: 'depositInfo',
          component: resolve => require(
            ['@/components/channelManage/depositInfo'], resolve),
          meta: {
            requireAuth: true,
          },
        },
        {
          path: '/channelContract',//合同管理
          name: 'channelContract',
          component: resolve => require(
            ['@/components/channelManage/channelContract'], resolve),
          meta: {
            requireAuth: true,
          },
        },
        {
          path: '/channelContractUpload/:channelId/:channelCode/:channelName/:contractId',//合同管理
          name: 'channelContractUpload',
          component: resolve => require(
            ['@/components/channelManage/channelContractUpload'], resolve),
          meta: {
            requireAuth: true,
          },
        },
        {
          path: '/contactInfo',//合同信息
          name: 'contactInfo',
          meta: {
            requireAuth: true,
          },
          component: resolve => require(
            ['@/components/channelManage/contactInfo'], resolve),
        },
        {
          path: '/channelBackList',//黑名单管理
          name: 'channelBackList',
          component: resolve => require(
            ['@/components/channelManage/channelBackList'], resolve),
          meta: {
            requireAuth: true,
          },
        },
        {
          path: '/productMaintenance',//产品信息维护
          name: 'productMaintenance',
          component: resolve => require(
            ['@/components/productManage/productMaintenance'], resolve),
          meta: {
            requireAuth: true,
          },
        },
        {
          path: '/productAdded',//产品信息维护
          name: 'productAdded',
          component: resolve => require(
            ['@/components/productManage/productAdded'], resolve),
          meta: {
            requireAuth: true,
          },
        },
        {
          path: '/productUpdate',//产品信息修改
            name: 'productUpdate',
          component: resolve => require(
          ['@/components/productManage/productUpdate'], resolve),
          meta: {
          requireAuth: true,
        },
        },
        {
          path: '/productApproval',//产品审批
          name: 'productApproval',
          component: resolve => require(
            ['@/components/productManage/productApproval'], resolve),
          meta: {
            requireAuth: true,
          },
        },
        {
          path: '/productAudit',//产品审批
            name: 'productAudit',
          component: resolve => require(
          ['@/components/productManage/productAudit'], resolve),
          meta: {
          requireAuth: true,
        },
        },
        {
          path: '/loan',//进件信息查询
            name: 'loan',
          component: resolve => require(
          ['@/components/businessManage/loan'], resolve),
          meta: {
          requireAuth: true,
        },
        },
        {
          path: '/loanInfo/:policyNo',//进件信息查询
            name: 'loanInfo',
          component: resolve => require(
          ['@/components/businessManage/loanInfo'], resolve),
          meta: {
          requireAuth: true,
        },
        },
        {
          path: '/refunds',//退货管理
            name: 'refunds',
          component: resolve => require(
          ['@/components/businessManage/refunds'], resolve),
          meta: {
          requireAuth: true,
          },
        },
        {
          path: '/refundsInfo/:myPolicyNo',//退货详情
            name: 'refundsInfo',
          component: resolve => require(
          ['@/components/businessManage/refundsInfo'], resolve),
          meta: {
          requireAuth: true,
        },
        },
        /*{
          path: '/imageMaintenance',//影像信息维护
          name: 'imageMaintenance',
          component: resolve => require(['@/components/systemConfig/imageMaintenance'],resolve),
        },
        {
          path: '/dataDictionary',//数据字典维护
          name: 'dataDictionary',
          component: resolve => require(['@/components/systemConfig/dataDictionary'],resolve),
        },*/
        {
          path: '/userManage',//用户管理
          name: 'userMange',
          component: resolve => require(
            ['@/components/systemConfig/userManage'], resolve),
          meta: {
            requireAuth: true,
          },
        },
        {
          path: '/roleManage',//角色管理
          name: 'roleManage',
          component: resolve => require(
            ['@/components/systemConfig/roleManage'], resolve),
          meta: {
            requireAuth: true,
          },
        },
        {
          path: '/orderManage',//菜单管理
          name: 'orderManage',
          component: resolve => require(
            ['@/components/systemConfig/orderManage'], resolve),
          meta: {
            requireAuth: true,
          },
        },
        {
          path: '/creditAuto',//自动审批列表
            name: 'creditAuto',
          component: resolve => require(
          ['@/components/creditAudit/auditAuto'], resolve),
          meta: {
          requireAuth: true,
          },
        },
        {
          path: '/auditOrder',//人工审批订单
            name: 'auditOrder',
          component: resolve => require(
          ['@/components/creditAudit/auditOrder'], resolve),
          meta: {
          requireAuth: true,
          },
        },
        {
          path: '/myAuditList',//我的审批列表
            name: 'myAuditList',
            component: resolve => require(
            ['@/components/creditAudit/myAuditList'], resolve),
            meta: {
            requireAuth: true,
          },
        },
{
  path: '/contractList',//合同列表
    name: 'contractList',
  component: resolve => require(
  ['@/components/creditAudit/contractList'], resolve),
  meta: {
  requireAuth: true,
},
},
{
  path: '/settlementExport',//放款导出
    name: 'settlementExport',
  component: resolve => require(
  ['@/components/settlementManage/export'], resolve),
  meta: {
  requireAuth: true,
},
},
{
  path: '/settlementImport',//回盘导入
    name: 'settlementImport',
  component: resolve => require(
  ['@/components/settlementManage/import'], resolve),
  meta: {
  requireAuth: true,
},
},
{
  path: '/overdueRepay',//逾期还款
    name: 'overdueRepay',
  component: resolve => require(
  ['@/components/repayManage/overdueRepay'], resolve),
  meta: {
  requireAuth: true,
},
},
{
  path: '/repayInAdvance',//提前结清
    name: 'repayInAdvance',
  component: resolve => require(
  ['@/components/repayManage/repayInAdvance'], resolve),
  meta: {
  requireAuth: true,
},
},
{
  path: '/repayExport',//还款导出
    name: 'repayExport',
  component: resolve => require(
  ['@/components/repayManage/export'], resolve),
  meta: {
  requireAuth: true,
},
},
{
  path: '/repayImport',//还款回盘
    name: 'repayImport',
  component: resolve => require(
  ['@/components/repayManage/import'], resolve),
  meta: {
  requireAuth: true,
},
},
{
  path: '/repay',//正常还款
    name: 'repay',
  component: resolve => require(
  ['@/components/repayManage/repay'], resolve),
  meta: {
  requireAuth: true,
},
},
{
  path: '/blackList',//风控管理
    name: 'blackList',
  component: resolve => require(
  ['@/components/riskManagement/blackList'], resolve),
  meta: {
  requireAuth: true,
},
},
{
  path: '/productRule',//风控管理
    name: 'productRule',
  component: resolve => require(
  ['@/components/riskManagement/productRule'], resolve),
  meta: {
  requireAuth: true,
},
},
{
  path: '/gjModel',//风控管理
    name: 'gjModel',
  component: resolve => require(
  ['@/components/riskManagement/gjModel'], resolve),
  meta: {
  requireAuth: true,
},
},
{
  path: '/gjRule',//风控管理
    name: 'gjRule',
  component: resolve => require(
  ['@/components/riskManagement/gjRule'], resolve),
  meta: {
    requireAuth: true
  }
},
{
  path: '/overdueStep',//逾期管理
    name: 'overdueStep',
  component: resolve => require(
  ['@/components/overdueManage/overdueStep'], resolve),
  meta: {
    requireAuth: true
  }
},
{
  path: '/badDebt',//逾期管理
    name: 'badDebt',
  component: resolve => require(
  ['@/components/overdueManage/badDebt'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/customer',//客服管理
    name: 'customer',
  component: resolve => require(
  ['@/components/customerManage/customer'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/urgeGroup',//催收管理
    name: 'urgeGroup',
  component: resolve => require(
  ['@/components/urgeManage/urgeGroup'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/phoneUrge',//催收管理
    name: 'phoneUrge',
  component: resolve => require(
  ['@/components/urgeManage/phoneUrge'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/jmQuery',//催收管理
    name: 'jmQuery',
  component: resolve => require(
  ['@/components/urgeManage/jmQuery'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/zqQuery',//催收管理
    name: 'zqQuery',
  component: resolve => require(
  ['@/components/urgeManage/zqQuery'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/guarantorList',//担保管理
    name: 'guarantorList',
  component: resolve => require(
  ['@/components/guarantorManage/list'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/loanUser',//借款人查询
    name: 'loanUser',
  component: resolve => require(
  ['@/components/businessManage/loanUser'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/loanQuery',//放款查询
    name: 'loanQuery',
  component: resolve => require(
  ['@/components/businessManage/loanQuery'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/repayQuery',//还款查询
    name: 'repayQuery',
  component: resolve => require(
  ['@/components/businessManage/repayQuery'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/orverdueQuery',//逾期查询
    name: 'orverdueQuery',
  component: resolve => require(
  ['@/components/businessManage/orverdueQuery'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/customerQuery',//客服查询
    name: 'customerQuery',
  component: resolve => require(
  ['@/components/businessManage/customerQuery'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/urgeQuery',//催收查询
    name: 'urgeQuery',
  component: resolve => require(
  ['@/components/businessManage/urgeQuery'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/badDebtQuery',//坏账查询
    name: 'badDebtQuery',
  component: resolve => require(
  ['@/components/businessManage/badDebtQuery'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/blackQuery',//黑名单查询
    name: 'blackQuery',
  component: resolve => require(
  ['@/components/businessManage/blackQuery'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/distStati',//渠道商业务统计
    name: 'distStati',
  component: resolve => require(
  ['@/components/businessStatiManage/distStati'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/productStati',//产品销售统计
    name: 'productStati',
  component: resolve => require(
  ['@/components/businessStatiManage/productStati'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/loanReconciliation',//放款对账
    name: 'loanReconciliation',
  component: resolve => require(
  ['@/components/reconciliationManage/loanReconciliation'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/repayReconciliation',//收款对账
    name: 'repayReconciliation',
  component: resolve => require(
  ['@/components/reconciliationManage/repayReconciliation'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/depositList',//保证金审核
    name: 'depositList',
  component: resolve => require(
  ['@/components/auditManage/depositList'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/creditList',//渠道授信审批
    name: 'creditList',
  component: resolve => require(
  ['@/components/auditManage/creditList'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/customerBlackList',//客户黑名单变更审批
    name: 'customerBlackList',
  component: resolve => require(
  ['@/components/auditManage/customerBlackList'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/distBlackList',//渠道黑名单变更审批
    name: 'distBlackList',
  component: resolve => require(
  ['@/components/auditManage/distBlackList'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/distAdd',//渠道登记审批列表
    name: 'distAdd',
  component: resolve => require(
  ['@/components/auditManage/distAdd'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/contract',//合同审批列表
    name: 'contract',
  component: resolve => require(
  ['@/components/auditManage/contract'], resolve),
  meta: {
  requireAuth: true
}
},
{
  path: '/refundsAudit',//退货审批列表
    name: 'refundsAudit',
  component: resolve => require(
  ['@/components/auditManage/refunds'], resolve),
  meta: {
  requireAuth: true
}
},
        {
          path: '/center',//个人中心
            name: 'center',
          component: resolve => require(
          ['@/components/systemConfig/center'], resolve),
          meta: {
            requireAuth: true
          }
        }

      ]
    },
    {
      path: '/login',
      name: 'login',
      component: resolve => require(['@/components/login'], resolve)
    }
  ],
  //mode: 'history',
})
export default router;
