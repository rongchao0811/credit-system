import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

/**
 * 访问状态对象
 * SPA（单页应用程序）中的共享值
 * @type {{size: string}}
 */
const state = {
  size: "small",//控制整体UI显示大小 mini/small/medium
  channelId:'',//渠道详情查询id
  isEdit:0,//当前界面内容是否可编辑 0不可编辑
  channelName:'',
  jtEdit:0,//控制尽调是否可编辑,
  token:0,//判断是否登录
  nextStep:'0',//0为基本信息 1为影像信息
  editableTabsValue:'',//当前tab签的路由
  editableTabs:[]//所以tab签
}
/**
 * Mutations修改状态（$store.commit( )）
 * @type {{}}
 */
const mutations = {
  changeUISize(state,size){
    state.size = size
  },
  changeChannelId(state,crtId){
    state.channelId = crtId
  },
  changeEdit(state,v){
    state.isEdit = v
  },
  changeChannelName(state,v){
    state.channelName = v
  },
  changeJdEdit(state,v){
    state.jtEdit = v
  },
  changeToken(state,v){
    state.token=v
    window.sessionStorage.setItem('token',v)
  },
  changeStep(state,v){
    state.nextStep=v
  },
  pushTabs(state,v){
    state.editableTabs.push(v)
  },
  setEdtTab(state,v){
    state.editableTabsValue = v
  }
}

const actions = {

}

const getters = {

}
export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters
})
