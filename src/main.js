// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import router from './router'
import store from './vuex/store'
import "babel-polyfill";

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '../static/styles/main.css'
import apiLib from '../static/js/lib'
Vue.prototype.$axios = apiLib

import 'element-ui/lib/index'
import '../static/styles/main.css'
import * as filters from './filters/filter'

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  if (to.meta.requireAuth) {  // 需要权限,进一步进行判断
    if (store.state.token||window.sessionStorage.token=="1") {  // 通过vuex state获取当前的token是否存在
      next();
    }
    else {    //如果没有权限,重定向到登录页,进行登录
      next({
             path: '/login',
             query: {redirect: to.fullPath}  // 将跳转的路由path作为参数，登录成功后跳转到该路由
           })
    }
  }
  else { //不需要权限 直接跳转
    next();
  }
})




Vue.use(router)
Vue.use(ElementUI,{ size: 'mini' })
Vue.use(Vuex)
/*import VueAreaLinkage from 'vue-area-linkage';
Vue.use(VueAreaLinkage)*/
//axios.defaults.baseURL = 'http://test.credit.chezhubaitiao.com';

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})

Object.keys(filters).forEach(key => {
  console.log(key)
  Vue.filter(key, filters[key])
})
